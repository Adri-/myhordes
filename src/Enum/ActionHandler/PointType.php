<?php

namespace App\Enum\ActionHandler;

enum PointType: int {
    case AP = 1;
    case CP = 2;
    case MP = 3;
}